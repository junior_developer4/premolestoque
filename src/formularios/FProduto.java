/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * GrupoProduto.java
 *
 * Created on 06/11/2012, 11:12:21
 */
package formularios;

import dao.ConectaBanco;
import dao.GrupoProdutoDao;
import dao.ProdutoDao;
import entidades.GrupoProdutos;
import entidades.Produtos;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import utilitarios.ValidarValor;

/**
 *
 * @author cliente
 */
public class FProduto extends JanelaInternaSingleton {

    /** Creates new form GrupoProduto */
     private ProdutoDao pDao= new ProdutoDao();
     private GrupoProdutoDao gpDao= new GrupoProdutoDao();
     private TableRowSorter<TableModel> sorter=null;
    public FProduto() {
        
        initComponents();
        
        Tabela.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        DefaultTableColumnModel colunas = (DefaultTableColumnModel)Tabela.getColumnModel();
        colunas.getColumn(0).setPreferredWidth(50);
        colunas.getColumn(1).setPreferredWidth(350);
        
        sorter =new TableRowSorter<TableModel>(Tabela.getModel());
        Tabela.setRowSorter(sorter);
        
        dao.ConectaBanco.setBanco(ConectaBanco.POSTGRESQL);
         
        atualizaLista();
    }
    
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TituloFormulario = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        descproduto = new javax.swing.JTextField();
        codproduto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        unidade = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        estoquemax = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        estoquemim = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        estoqueatual = new javax.swing.JTextField();
        tipo = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        codgrupo = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        descgrupo = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabela = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        sair = new javax.swing.JButton();
        salvar = new javax.swing.JButton();
        excluir = new javax.swing.JButton();
        novo = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        codgrupo1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        descgrupo1 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        codgrupo2 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        descgrupo2 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        sair1 = new javax.swing.JButton();

        setTitle("Cadastro de Produtos");
        setMinimumSize(new java.awt.Dimension(500, 320));
        getContentPane().setLayout(null);

        TituloFormulario.setBackground(new java.awt.Color(0, 204, 204));
        TituloFormulario.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        TituloFormulario.setForeground(new java.awt.Color(255, 255, 255));
        TituloFormulario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        TituloFormulario.setText("Produtos");
        TituloFormulario.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        TituloFormulario.setOpaque(true);
        TituloFormulario.setPreferredSize(new java.awt.Dimension(289, 35));
        getContentPane().add(TituloFormulario);
        TituloFormulario.setBounds(0, 0, 620, 40);

        jPanel4.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);
        jPanel2.add(descproduto);
        descproduto.setBounds(80, 30, 440, 20);

        codproduto.setBackground(new java.awt.Color(255, 255, 153));
        codproduto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                codprodutoFocusLost(evt);
            }
        });
        jPanel2.add(codproduto);
        codproduto.setBounds(10, 30, 59, 20);

        jLabel3.setText("Descrição Produto");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(80, 10, 300, 14);

        jLabel4.setText("Código");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(10, 10, 60, 14);

        jLabel5.setText("Unidade");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(530, 10, 50, 14);
        jPanel2.add(unidade);
        unidade.setBounds(530, 30, 59, 20);

        jLabel6.setText("Estoque Max.");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(10, 60, 90, 14);
        jPanel2.add(estoquemax);
        estoquemax.setBounds(10, 80, 90, 20);

        jLabel7.setText("Estoque Mim.");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(110, 60, 90, 14);
        jPanel2.add(estoquemim);
        estoquemim.setBounds(110, 80, 90, 20);

        jLabel8.setText(" Tipo");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(300, 60, 90, 14);

        estoqueatual.setBackground(new java.awt.Color(255, 255, 153));
        estoqueatual.setEditable(false);
        jPanel2.add(estoqueatual);
        estoqueatual.setBounds(210, 80, 80, 20);

        tipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1 - Produto comercial", "2 - Produto Indutrializado", " " }));
        jPanel2.add(tipo);
        tipo.setBounds(300, 80, 290, 22);

        jLabel15.setText("Estoque Atual");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(210, 60, 90, 14);

        jPanel4.add(jPanel2);
        jPanel2.setBounds(10, 10, 600, 110);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        jLabel9.setText("Código");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(10, 10, 50, 14);

        codgrupo.setBackground(new java.awt.Color(255, 255, 153));
        codgrupo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                codgrupoFocusLost(evt);
            }
        });
        jPanel3.add(codgrupo);
        codgrupo.setBounds(10, 30, 59, 20);

        jLabel10.setText("Grupo Produto");
        jPanel3.add(jLabel10);
        jLabel10.setBounds(70, 10, 300, 14);

        descgrupo.setBackground(new java.awt.Color(255, 255, 204));
        descgrupo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                descgrupoKeyReleased(evt);
            }
        });
        jPanel3.add(descgrupo);
        descgrupo.setBounds(70, 30, 520, 20);

        jPanel4.add(jPanel3);
        jPanel3.setBounds(10, 130, 600, 60);

        Tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Produto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        Tabela.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabelaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(Tabela);
        Tabela.getColumnModel().getColumn(0).setMinWidth(60);
        Tabela.getColumnModel().getColumn(0).setPreferredWidth(60);
        Tabela.getColumnModel().getColumn(0).setMaxWidth(60);

        jPanel4.add(jScrollPane1);
        jScrollPane1.setBounds(10, 200, 600, 130);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        sair.setBackground(new java.awt.Color(232, 231, 231));
        sair.setText("Sair");
        sair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sairActionPerformed(evt);
            }
        });
        jPanel1.add(sair);
        sair.setBounds(510, 5, 80, 30);

        salvar.setBackground(new java.awt.Color(232, 231, 231));
        salvar.setText("Salvar");
        salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salvarActionPerformed(evt);
            }
        });
        jPanel1.add(salvar);
        salvar.setBounds(90, 5, 70, 30);

        excluir.setBackground(new java.awt.Color(232, 231, 231));
        excluir.setText("Excluir");
        excluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluirActionPerformed(evt);
            }
        });
        jPanel1.add(excluir);
        excluir.setBounds(170, 5, 80, 30);

        novo.setBackground(new java.awt.Color(232, 231, 231));
        novo.setText("Novo");
        novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                novoActionPerformed(evt);
            }
        });
        jPanel1.add(novo);
        novo.setBounds(10, 5, 70, 30);

        jPanel4.add(jPanel1);
        jPanel1.setBounds(10, 340, 600, 40);

        jTabbedPane1.addTab("Cadastro", jPanel4);

        jPanel5.setLayout(null);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setLayout(null);

        jLabel11.setText("Código");
        jPanel6.add(jLabel11);
        jLabel11.setBounds(10, 10, 50, 14);

        codgrupo1.setBackground(new java.awt.Color(255, 255, 153));
        codgrupo1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                codgrupo1FocusLost(evt);
            }
        });
        jPanel6.add(codgrupo1);
        codgrupo1.setBounds(10, 30, 50, 20);

        jLabel12.setText(" Produto");
        jPanel6.add(jLabel12);
        jLabel12.setBounds(70, 10, 300, 14);

        descgrupo1.setBackground(new java.awt.Color(255, 255, 204));
        descgrupo1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                descgrupo1KeyReleased(evt);
            }
        });
        jPanel6.add(descgrupo1);
        descgrupo1.setBounds(60, 30, 240, 20);

        jLabel13.setText("Código");
        jPanel6.add(jLabel13);
        jLabel13.setBounds(310, 10, 50, 14);

        codgrupo2.setBackground(new java.awt.Color(255, 255, 153));
        codgrupo2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                codgrupo2FocusLost(evt);
            }
        });
        jPanel6.add(codgrupo2);
        codgrupo2.setBounds(310, 30, 50, 20);

        jLabel14.setText("Grupo  Produto");
        jPanel6.add(jLabel14);
        jLabel14.setBounds(370, 10, 300, 14);

        descgrupo2.setBackground(new java.awt.Color(255, 255, 204));
        descgrupo2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                descgrupo2KeyReleased(evt);
            }
        });
        jPanel6.add(descgrupo2);
        descgrupo2.setBounds(360, 30, 230, 20);

        jPanel5.add(jPanel6);
        jPanel6.setBounds(10, 10, 600, 60);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Código", "Produto", "Und.", "Estoque"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jPanel5.add(jScrollPane2);
        jScrollPane2.setBounds(0, 80, 610, 250);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(null);

        sair1.setBackground(new java.awt.Color(232, 231, 231));
        sair1.setText("Sair");
        sair1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sair1ActionPerformed(evt);
            }
        });
        jPanel7.add(sair1);
        sair1.setBounds(510, 5, 80, 30);

        jPanel5.add(jPanel7);
        jPanel7.setBounds(10, 340, 600, 40);

        jTabbedPane1.addTab("Consulta", jPanel5);

        getContentPane().add(jTabbedPane1);
        jTabbedPane1.setBounds(0, 40, 620, 410);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-625)/2, (screenSize.height-479)/2, 625, 479);
    }// </editor-fold>//GEN-END:initComponents

private void sairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sairActionPerformed
dispose();
}//GEN-LAST:event_sairActionPerformed

private void salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salvarActionPerformed
    try {
        int codigo = 0;
        if (descproduto.getText().length() > 0) {

            dao.ConectaBanco.setBanco(ConectaBanco.POSTGRESQL);
            Produtos p = new Produtos();
            GrupoProdutos gp = gpDao.ConsultarEnt(ValidarValor.getInt(codgrupo.getText()));

            System.out.println("salvar gp 00 ");
            if (ValidarValor.getInt(codproduto.getText()) > 0) {
                p = pDao.ConsultarEnt(Integer.parseInt(codproduto.getText()));
                System.out.println("salvar gp 000 ");
            } else {
                p = null;
            }
            if (p == null) {
                p = new Produtos();
                System.out.println("salvar gp 01 ");
                p.setProduto(descproduto.getText());
                p.setUnidade(unidade.getText());
                p.setEstoquemax(Double.valueOf(estoquemax.getText()));
                p.setEstoquemin(Double.valueOf(estoquemim.getText()));
                p.setEstoqueatual(ValidarValor.getDouble(estoqueatual.getText()));
                p.setTipo(tipo.getSelectedIndex());
                p.setGrupo(gp);
                codigo = pDao.criar(p);
                atualizaLista();
            } else {
                System.out.println("salvar gp 02 ");
                 p.setProduto(descproduto.getText());
                p.setUnidade(unidade.getText());
                p.setEstoquemax(Double.valueOf(estoquemax.getText()));
                p.setEstoquemin(Double.valueOf(estoquemim.getText()));
                p.setTipo(tipo.getSelectedIndex());
                p.setGrupo(gp);
                pDao.atualizar(p);
                codigo = p.getCodigo();
                 atualizaLista();
            }
        }
        codproduto.setText("" + codigo);

    } catch (Exception e) {
        e.printStackTrace();
    }
}//GEN-LAST:event_salvarActionPerformed

private void codprodutoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codprodutoFocusLost
buscaProduto(ValidarValor.getInt(codproduto.getText()));    
}//GEN-LAST:event_codprodutoFocusLost


private void buscaProduto(int codigo){
      try {

            Produtos p = new Produtos();

            dao.ConectaBanco.setBanco(ConectaBanco.POSTGRESQL);

            p = pDao.ConsultarEnt(codigo);
            if (p != null) {
                descproduto.setText(p.getProduto());
                unidade.setText(p.getUnidade());
                estoquemax.setText(""+p.getEstoquemax());
                estoquemim.setText(p.getEstoquemin().toString());
                estoqueatual.setText(p.getEstoqueatual().toString());
                codgrupo.setText(""+p.getGrupo().getCodigo());
                tipo.setSelectedIndex(p.getTipo());
                buscaGrupo(p.getGrupo().getCodigo());
                
                        
            } else {
                limpar();
            }


        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Erro ao tentar buscar dados!\n"+e);
        }
}

  private void buscaGrupo(int codigo){
         try {

            GrupoProdutos gp = new GrupoProdutos();

            dao.ConectaBanco.setBanco(ConectaBanco.POSTGRESQL);

            gp = gpDao.ConsultarEnt(codigo);
            if (gp != null) {
                descgrupo.setText(gp.getGrupo());
            } else {
                descgrupo.setText("");
            }


        } catch (Exception e) {
            e.printStackTrace();
             JOptionPane.showMessageDialog(this, "Erro ao tentar buscar dados!\n"+e);
        }
         
    }
  
  
private void codgrupoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codgrupoFocusLost
  buscaGrupo(ValidarValor.getInt(codgrupo.getText()));
}//GEN-LAST:event_codgrupoFocusLost

private void excluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluirActionPerformed
   try {

            Produtos p = new Produtos();
            p.setCodigo(ValidarValor.getInt(codproduto.getText()));
            if (JOptionPane.showConfirmDialog(null, "Deseja deletar o grupo: " + descproduto.getText().trim() + "?", "CONFIRMAÇÃO", JOptionPane.YES_NO_OPTION) == 0) {
                dao.ConectaBanco.setBanco(ConectaBanco.POSTGRESQL);
                pDao.apagar(p);
                limpar();
                atualizaLista();
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Erro ao tentar excluir dados!\n" + e);
        }
}//GEN-LAST:event_excluirActionPerformed

    private void novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_novoActionPerformed
       limpar();
       descproduto.requestFocus();
    }//GEN-LAST:event_novoActionPerformed

    private void TabelaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabelaMouseClicked
       codproduto.setText(Tabela.getValueAt(Tabela.getSelectedRow(), 0).toString());
       buscaProduto(ValidarValor.getInt(codproduto.getText()));
    }//GEN-LAST:event_TabelaMouseClicked

    private void descgrupoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descgrupoKeyReleased
       
    }//GEN-LAST:event_descgrupoKeyReleased

    private void codgrupo1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codgrupo1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_codgrupo1FocusLost

    private void descgrupo1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descgrupo1KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_descgrupo1KeyReleased

    private void codgrupo2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codgrupo2FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_codgrupo2FocusLost

    private void descgrupo2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descgrupo2KeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_descgrupo2KeyReleased

    private void sair1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sair1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sair1ActionPerformed

    private void limpar(){
        codproduto.setText("");
        descgrupo.setText("");
        descproduto.setText("");
        unidade.setText("");
        estoqueatual.setText("");
        estoquemax.setText("");
        estoquemim.setText("");
        codgrupo.setText("");
    }
   
     private void atualizaLista() {
        DefaultTableModel model = (DefaultTableModel) Tabela.getModel();

        Produtos p = null;

        List<Produtos> lista = null;
        lista = pDao.buscar(p);
        dao.ConectaBanco.setBanco(ConectaBanco.POSTGRESQL);

        model.setNumRows(lista.size());
        int contador = 0;
        for (contador = 0; contador < lista.size(); contador++) {

            model.setValueAt(lista.get(contador).getCodigo(), contador, 0);
            model.setValueAt(lista.get(contador).getProduto(), contador, 1);

        }

    }
     
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Tabela;
    private javax.swing.JLabel TituloFormulario;
    private javax.swing.JTextField codgrupo;
    private javax.swing.JTextField codgrupo1;
    private javax.swing.JTextField codgrupo2;
    private javax.swing.JTextField codproduto;
    private javax.swing.JTextField descgrupo;
    private javax.swing.JTextField descgrupo1;
    private javax.swing.JTextField descgrupo2;
    private javax.swing.JTextField descproduto;
    private javax.swing.JTextField estoqueatual;
    private javax.swing.JTextField estoquemax;
    private javax.swing.JTextField estoquemim;
    private javax.swing.JButton excluir;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton novo;
    private javax.swing.JButton sair;
    private javax.swing.JButton sair1;
    private javax.swing.JButton salvar;
    private javax.swing.JComboBox tipo;
    private javax.swing.JTextField unidade;
    // End of variables declaration//GEN-END:variables
}
