/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author cliente
 */
public class EntradaItens  {
    
   private Entradas entrada;
   private Produtos produto;
   private Double valorUnitario;
   private Double qtde;
   private int codigo;
   private List l = null;
    
    
    
    public EntradaItens(){
    
    }
    
     public EntradaItens(int codigo){
         this.codigo=codigo;
    }
    
    public EntradaItens(int codigo, Produtos produto, Entradas entrada, Double qtde, Double valorUnitario){
        this.entrada=entrada;
        this.produto=produto;
        this.codigo=codigo;
        this.qtde=qtde;
        this.valorUnitario=valorUnitario;
              
    
    }
    
    public EntradaItens (List l){
    this.l=l;
    }

    public List getL() {
        return l;
    }

    public void setL(List l) {
        this.l = l;
    }
   

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Entradas getEntrada() {
        return entrada;
    }

    public void setEntrada(Entradas entrada) {
        this.entrada = entrada;
    }

   
    public Produtos getProduto() {
        return produto;
    }

    public void setProduto(Produtos produto) {
        this.produto = produto;
    }

    public Double getQtde() {
        return qtde;
    }

    public void setQtde(Double qtde) {
        this.qtde = qtde;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

     
     
    
}
