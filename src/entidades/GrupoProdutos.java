/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author cliente
 */
public class GrupoProdutos {
    
    private int codigo; 
    private String grupo;
    
    public GrupoProdutos(){
    
    }
    
     public GrupoProdutos(int codigo){
        this.codigo=codigo;
    }
     
     public GrupoProdutos(String grupo){
        this.grupo=grupo;
    }
     
     public GrupoProdutos(int codigo, String grupo){
        this.codigo=codigo;
        this.grupo=grupo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
     
     
    
}
