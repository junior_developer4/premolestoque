/*
 * ValidarValor.java
 *
 * Created on 18 de Novembro de 2012, 11:36
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package utilitarios;




import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat; 
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author adm
 */
public class ValidarValor {

     //DateFormat df = new SimpleDateFormat(FORMAT_DATA_BR);
     public static String FORMAT_DATA="yyyy-MM-dd"; //Formato de data do MySQL
    public static String FORMAT_DATA_BR="dd/MM/yyyy";
    public static String FORMATO_FUNCAO_DATA="NOW();"; //GETDATE()
    public static String FORMAT_DATATIME_BR="dd/MM/yyyy HH:mm:ss";
    
    /** Creates a new instance of ValidarValor */
    public ValidarValor() {
    }
    
    public static String getDate(java.util.Date date){
        String FvaRetorno="";
        if(date==null) return "";
        try{            
            DateFormat df = new SimpleDateFormat(FORMAT_DATA_BR);
            df.setTimeZone(TimeZone.getTimeZone("GMT -03:00"));
            df.setLenient(false);
            FvaRetorno=df.format(date);
        }catch(Exception e){
            System.out.println("Ocorreu no método getDate(java.sql.date) ao tentar formatar a data informada. Erro: "+e);
//            e.printStackTrace();
        }
        return FvaRetorno;
    }
    
    public static boolean isValor(String valor){
        boolean FvaRetorno=false;
        try{
            DecimalFormat  formatador  = new DecimalFormat();
            formatador.applyPattern("###,####,##0.00;-###,###,##0.00");
            double f=formatador.parse(valor.trim()).doubleValue();
            FvaRetorno=true;
        }catch(Exception e){
            System.out.println("Erro ao tentar validar o valor informado. Erro: "+e);
        }
        return FvaRetorno;
    }
    
    
     public static String getDateTime(Date date) {
        
        String FvaRetorno="";
        if(date==null) return "";
        try{
            java.util.Date newDate=new java.util.Date();
            newDate.setTime(date.getTime());
            
            DateFormat df = new SimpleDateFormat(FORMAT_DATATIME_BR);
            df.setLenient(false);
            FvaRetorno=df.format(newDate);
        }catch(Exception e){
            System.out.println("Ocorreu no mï¿½todo getDate(java.sql.date) ao tentar formatar a data informada. Erro: "+e);
//            e.printStackTrace();
        }
        return FvaRetorno;
    }  
    
    public static String getDateTimeSQL(java.sql.Date date) {       
        String FvaRetorno="";
        if(date==null) return "";
        try{
            java.util.Date newDate=new java.util.Date();
            newDate.setTime(date.getTime());
            
            DateFormat df = new SimpleDateFormat(FORMAT_DATATIME_BR);
            df.setLenient(false);
            FvaRetorno=df.format(newDate);
            
        }catch(Exception e){
            System.out.println("Ocorreu no mï¿½todo getDate(java.sql.date) ao tentar formatar a data informada. Erro: "+e);
//            e.printStackTrace();
        }
        return FvaRetorno;
    }
    
    public static java.sql.Date getDateTimeSQL(String dataStr){
        java.sql.Date FvaDataRetorno=null;
        try{
            SimpleDateFormat df = new SimpleDateFormat(FORMAT_DATATIME_BR);   
            df.setLenient(false);
            FvaDataRetorno=new java.sql.Date( df.parse(dataStr).getTime());
        }catch(Exception e){
            System.out.println("Ocorreu um erro na fun��o getDateParse(String dataStr, String pattern). Erro: "+e);
        }
        return FvaDataRetorno;
    }
    
    public static java.util.Date getDateUtil(String dateStr){
        java.util.Date FvaRetorno=null;
        dateStr=dateStr+"";
        if(dateStr.equalsIgnoreCase("null") || dateStr.length()==0) return null;
        try{
            java.util.Date newDate;
            DateFormat df = new SimpleDateFormat(FORMAT_DATA_BR);
            df.setLenient(false);
            newDate=df.parse(dateStr);
            
            FvaRetorno=new java.util.Date(newDate.getTime());
        }catch(Exception e){
            System.out.println("Ocorreu no m�todo getDateSQL(String dateStr) ao tentar formatar a data informada. Erro: "+e);
//            e.printStackTrace();
        }
        return FvaRetorno;
    }

    public static double getDouble(String valor){
        double FvaRetorno=0.0;
        if(valor.trim().length()>0){
            try{
                DecimalFormat  formatador  = new DecimalFormat();
                formatador.applyPattern("###,####,##0.00;-###,###,##0.00");
                FvaRetorno=formatador.parse(valor.trim()).doubleValue();
            }catch(Exception e){
                System.out.println("Erro ao tentar converter o valor informado. Erro: "+e);
            }
        }
        return FvaRetorno;
    }
    
     public static double getBigD(String valor){
        double FvaRetorno=0.0;
        if(valor.trim().length()>0){
            try{
                DecimalFormat  formatador  = new DecimalFormat();
                formatador.applyPattern("###,####,##0.0000;-###,###,##0.0000");
                FvaRetorno=formatador.parse(valor.trim()).doubleValue();
            }catch(Exception e){
                System.out.println("Erro ao tentar converter o valor informado. Erro: "+e);
            }
        }
        return FvaRetorno;
    }
     
    public static String getDouble(double valor){
        String FvaRetorno="0,00";
        try{
            DecimalFormat  formatador  = new DecimalFormat();
            formatador.applyPattern("###,####,##0.00;-###,###,##0.00");
            FvaRetorno=formatador.format(valor);
        }catch(Exception e){
            System.out.println("Erro ao tentar converter o valor informado. Erro: "+e);
        }
        return FvaRetorno;
    }
  
    public static Double getParseDouble(String valor){
        Double FvaRetorno=0.0;
        try{            
            FvaRetorno=Double.parseDouble(valor);
        }catch(Exception e){
            System.out.println("Erro ao tentar converter o valor informado. Erro: "+e);
        }
        return FvaRetorno;
    }
    public static int getInt(String valor){
        
        int FvaRetorno=0;
        try{
            FvaRetorno=Integer.parseInt(valor.trim());
        }catch(Exception e){
            System.out.println("Erro ao tentar converter string em inteiro. Erro: "+e);
        }
        return FvaRetorno;
    }

    public static boolean getBoolean(String valor){
        boolean FvaRetorno=false;
        try{
            FvaRetorno=Boolean.parseBoolean(valor.trim());
        }catch(Exception e){
            System.out.println("Erro ao tentar converter string em inteiro. Erro: "+e);
        }
        return FvaRetorno;
    }

    public static long getLong(String valor){
        long FvaRetorno=0;
        try{
            FvaRetorno=Long.parseLong(valor.trim());
        }catch(Exception e){
            System.out.println("Erro ao tentar converter string em long. Erro: "+e);
        }
        return FvaRetorno;
    }
    
    public static Integer getInteger(String valor){
        int FvaRetorno=0;
        try{
            FvaRetorno=Integer.parseInt(valor.trim());
        }catch(Exception e){
            System.out.println("Erro ao tentar converter string em inteiro. Erro: "+e);
        }
        return FvaRetorno;
    }
    
    public static boolean isNumeric(String FvaValor){
        boolean isValido=true;
        FvaValor=(""+FvaValor).trim();
        if (FvaValor.length()>0){
            for (int i=0;i<FvaValor.length();i++){
                Character caracter=FvaValor.charAt(i);
                if (!Character.isDigit(caracter)){
                    isValido=false;
                }
            }
        }else isValido=false;
        return isValido;
    }
        
    public static String getFormatadoZE(int FvaValor,String mascara){
        String FvaRetorno="";
        DecimalFormat  formatador  = new DecimalFormat();
        formatador.applyPattern(mascara);
        try{
            FvaRetorno=formatador.format(new java.math.BigDecimal(FvaValor));
        }catch(Exception e){
            FvaRetorno="";
        }
        return FvaRetorno;
    }
    
    public static double getArredondamentoOFI(double valor){  //Utilizado no arredondamento de of parcial 04/06/2012
        valor=valor-0.001;
        return getArredondamento(valor, 2);
    } 
    
    public static double getArredondamento(double valor){  
        return getArredondamento(valor, 2);
    }  
    public static double getArredondamento(double valor, int precisao){  
        valor=valor+0.000000001;
        BigDecimal bd = new BigDecimal(valor);  
        bd = bd.setScale(precisao,BigDecimal.ROUND_HALF_UP);  
        valor = bd.doubleValue();  
        return valor;
    }  
    
    public static String getValorFormatadoAB(String vr){///  654879.60
        String ret = vr.substring(0,(vr.length()-2)) + "." + vr.substring(vr.length()-2);
        return ret;
    }
}
